// 注意: 必須使用"Free Pascal v2.4.4 Complier"或更新版本的編譯器進行編譯; 否則因部分unit不存在而導致編譯"失敗"!!! //
PROGRAM s20061618;
USES
	CRT, SYSUTILS, MD5;
CONST
	{ general }
	FILES_quantity = 2; //檔案數量
	FILES_personnel = 'personnel.txt'; //用戶檔案名稱
	FILES_dictionary = 'dictionary.txt'; //字典檔案名稱
	{ personnel }
	USERS_MAXquantity = 100; //最大用戶數目
	USERS_MAXretry = 3; //每次登入可重試次數
	{ dictionary }
	WORDS_MAXspan = 32; //每一詞彙最大長度
	WORDS_MAXquantity = 10000; //最大詞彙數目
	WORDS_MINquestion = 10; //最少可選擇問題數目
	WORDS_MAXquestion = 50; //最多可選擇問題數目
	WORDS_MAXretry = 3; //每一問題可重試次數
TYPE
	{ personnel }
	USERS_type =	RECORD
						Name : string[16];
						Code : string[32];
						Identity : string[8];
						Question : integer;
						Correct : integer;
						Score : integer;
					END;
	{ dictionary }
	WORDS_type =	RECORD
						Grade : 0..4;
						Part : 0..5;
						Vocab : string[WORDS_MAXspan];
						Used : boolean;
					END;
VAR
	{ general }
	Droot, Aroot, Mode : string;
	Haphazard, Useless : integer;
	Result : boolean;
	{ general-prompt }
	Pcolor : integer;
	Ptype, Ptext : string;
	Pkey : boolean;
	{ general-verify }
	ISletter, ISnumber, ISroot, Valid : boolean;
	{ personnel }
	USERS_array : array[1..USERS_MAXquantity] of USERS_type;
	USERS_present : string;
	USERS_quantity : integer;
	{ dictionary }
	WORDS_array : array[1..WORDS_MAXquantity] of WORDS_type;
	WORDS_quantity : integer;

PROCEDURE Prompt(var Pcolor : integer;var Ptype, Ptext : string;var Pkey : boolean);
BEGIN
	IF NOT (Pcolor = -1) THEN
	BEGIN
		writeln;
		textbackground(Pcolor);
		IF PColor IN [6, 7] THEN
			textcolor(0)
		ELSE
			textcolor(7);
		write('## ', Ptype, ': ');
		writeln(Ptext);
		textbackground(0);
		textcolor(7);
		IF Pkey THEN
		BEGIN
			writeln('PRESS [ANY KEY] TO CONTINUE...');
			readkey;
			delay(100);
			clrscr;
		END;
	END;
	Pcolor := -1;
	Ptype := '';
	Ptext := '';
	Pkey := FALSE;
END;

PROCEDURE Verify(Input : string;var ISletter, ISnumber, ISroot : boolean);
VAR
	N, I, Character : integer;
BEGIN
	Valid := FALSE;
	I := 0;
	IF I = 0 THEN
		FOR N := 1 TO length(Input) DO
		BEGIN
			Character := ord(Input[N]);
			IF ISletter THEN
				IF (Character IN [65..90]) OR (Character IN [97..122]) THEN
					I := I + 1;
			IF ISnumber THEN
				IF Character IN [48..57] THEN
					I := I + 1;
			IF ISroot THEN
				IF Character IN [92, 58, 32, 38, 126, 33, 64, 35, 36, 37, 94, 40, 41, 95, 43, 123, 125, 96, 45, 61, 91, 93, 59, 39, 46, 44] THEN
					I := I + 1
				ELSE
					IF Character IN [47, 42, 63, 34, 60, 62, 124] THEN
						I := -1;
		END;
	IF NOT (length(Input) = 0) THEN
		IF I = length(Input) THEN
			Valid := TRUE
		ELSE
		BEGIN
			Pcolor := 4;
			Ptype := 'ERROR';
			IF (ISroot) AND (length(Input) < 3) THEN
				Ptext := 'MUST LONGER THAN 3 CHARACTERS.'
			ELSE
				Ptext := 'FOUND UNACCEPTED CHARACTER.';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END;
	ISletter := FALSE;
	ISnumber := FALSE;
	ISroot := FALSE;
END;

PROCEDURE Directory;
VAR
	N : integer;
	Input, FILES_name : string;
	Error : boolean;
BEGIN
	Droot := '';
	Aroot := '';
	IF Droot = '' THEN
		Droot := getcurrentdir + '\';
	IF Aroot = '' THEN
		REPEAT
			clrscr;
			Pcolor := 7;
			Ptype := 'ASKING';
			Ptext := 'ABSOLUTE PATH? [ENGLISH ONLY]';
			Pkey := FALSE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
			textbackground(1);
			writeln('LEAVE BLANK FOR CURRENT PATH.');
			textbackground(0);
			readln(Input);
			IF length(Input) = 0 THEN
				Aroot := Droot
			ELSE
			BEGIN
				ISletter := TRUE;
				ISnumber := TRUE;
				ISroot := TRUE;
				Verify(Input, ISletter, ISnumber, ISroot);
				IF Valid THEN
				BEGIN
					Aroot := Input;
					IF NOT (Input[length(Input)] = '\') THEN
						Aroot := Aroot + '\';
				END;
			END;
			IF NOT (Aroot = '') THEN
			BEGIN
				N := 0;
				REPEAT
					N := N + 1;
					CASE N OF
						1 : FILES_name := FILES_personnel;
						2 : FILES_name := FILES_dictionary;
					END;
					Error := fileexists(concat(Aroot, FILES_name));
				UNTIL (NOT Error) OR (N = FILES_quantity);
				IF NOT Error THEN
				BEGIN
					Aroot := '';
					Pcolor := 4;
					Ptype := 'ERROR';
					Ptext := 'AT LEAST ONE FILE MISSING!';
					Pkey := TRUE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
				END
				ELSE
					IF N = FILES_quantity THEN
					BEGIN
						Pcolor := 2;
						Ptype := 'NOTICE';
						Ptext := 'ALL FILES EXIST!';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END;
			END;
		UNTIL (NOT (Aroot = '')) AND (Error);
END;

PROCEDURE FILES_io(var Mode : string);
VAR
	Import, Export : text;

FUNCTION USERS_sort : boolean;
VAR
	USERS_swap : USERS_type;
	N, I, R : integer;
BEGIN
	USERS_sort := FALSE;
	{ personnel }
	FOR N := 1 TO (USERS_quantity - 1) DO
	BEGIN
		USERS_swap := USERS_array[N + 1];
		I := 1;
		FOR R := 1 TO N DO
			IF USERS_swap.Score < USERS_array[R].Score THEN
				I := R + 1;
		FOR R := N DOWNTO I DO
			USERS_array[R + 1] := USERS_array[R];
		USERS_array[I] := USERS_swap;
	END;
	USERS_sort := TRUE;
END;

FUNCTION USERS_load : boolean;
VAR
	N : integer;
	Input : string;
BEGIN
	USERS_load := FALSE;
	{ personnel }
	USERS_present := '';
	FOR N := 1 TO USERS_MAXquantity DO
		WITH USERS_array[N] DO
		BEGIN
			Name := '';
			Code := '';
			Identity := 'unknown';
			Question := -1;
			Correct := -1;
			Score := -1;
		END;
	assign(Import, concat(Aroot, FILES_personnel));
	reset(Import);
	USERS_quantity := 0;
	WHILE NOT eof(Import) DO
	BEGIN
		USERS_quantity := USERS_quantity + 1;
		WITH USERS_array[USERS_quantity] DO
		BEGIN
			readln(Import, Name);
			readln(Import, Code);
			readln(Import, Input);
			IF (Input = 'guest') OR (Input = 'teens') OR (Input = 'folks') THEN
			BEGIN
				Identity := Input;
				IF (Identity = 'guest') OR (Identity = 'teens') THEN
					readln(Import, Question, Correct, Score);
			END;
		END;
	END;
	close(Import);
	USERS_load := TRUE;
END;

FUNCTION USERS_save : boolean;
VAR
	N : integer;
BEGIN
	USERS_save := FALSE;
	{ personnel }
	assign(Export, concat(Aroot, FILES_personnel));
	rewrite(Export);
	FOR N := 1 TO USERS_quantity DO
		WITH USERS_array[N] DO
			IF NOT (Name = '') THEN
			BEGIN
				writeln(Export, Name);
				writeln(Export, Code);
				writeln(Export, Identity);
				IF (Identity = 'guest') OR (Identity = 'teens') THEN
					writeln(Export, Question, ' ', Correct, ' ', Score);
			END;
	close(Export);
	USERS_save := TRUE;
END;

FUNCTION WORDS_random : boolean;
VAR
	WORDS_swap : WORDS_type;
	N : integer;
BEGIN
	WORDS_random := FALSE;
	{ dictionary }
	FOR N := 1 TO WORDS_quantity DO
		WITH WORDS_array[N] DO
			Used := FALSE;
	FOR N := 1 TO WORDS_quantity DO
	BEGIN
		Haphazard := random(WORDS_quantity) + 1;
		WORDS_swap := WORDS_array[Haphazard];
		WORDS_array[Haphazard] := WORDS_array[N];
		WORDS_array[N] := WORDS_swap;
	END;
	WORDS_random := TRUE;
END;

FUNCTION WORDS_sort : boolean;
VAR
	WORDS_swap : WORDS_type;
	N, I, R : integer;
BEGIN
	WORDS_sort := FALSE;
	{ dictionary }
	FOR N := 1 TO (WORDS_quantity - 1) DO
	BEGIN
		WORDS_swap := WORDS_array[N + 1];
		I := 1;
		FOR R := 1 TO N DO
			IF lowercase(WORDS_swap.Vocab) > lowercase(WORDS_array[R].Vocab) THEN
				I := R + 1;
		FOR R := N DOWNTO I DO
			WORDS_array[R + 1] := WORDS_array[R];
		WORDS_array[I] := WORDS_swap;
	END;
	WORDS_sort := TRUE;
END;

FUNCTION WORDS_load : boolean;
VAR
	N : integer;
BEGIN
	WORDS_load := FALSE;
	{ dictionary }
	FOR N := 1 TO WORDS_MAXquantity DO
		WITH WORDS_array[N] DO
		BEGIN
			Grade := 0;
			Part := 0;
			Vocab := '';
			Used := FALSE;
		END;
	assign(Import, concat(Aroot, FILES_dictionary));
	reset(Import);
	WORDS_quantity := 0;
	WHILE NOT eof(Import) DO
	BEGIN
		WORDS_quantity := WORDS_quantity + 1;
		WITH WORDS_array[WORDS_quantity] DO
		BEGIN
			readln(Import, Grade, Part);
			readln(Import, Vocab);
		END;
	END;
	close(Import);
	WORDS_load := TRUE;
END;

FUNCTION WORDS_save : boolean;
VAR
	N : integer;
BEGIN
	WORDS_save := FALSE;
	{ dictionary }
	assign(Export, concat(Aroot, FILES_dictionary));
	rewrite(Export);
	FOR N := 1 TO WORDS_quantity DO
		WITH WORDS_array[N] DO
			IF NOT (Vocab = '') THEN
			BEGIN
				writeln(Export, Grade, ' ', Part);
				writeln(Export, Vocab);
			END;
	close(Export);
	WORDS_save := TRUE;
END;

BEGIN
	clrscr;
	IF Mode = 'inital' THEN
	BEGIN
		USERS_load;
		WORDS_load;
	END
	ELSE
		IF Mode = 'backup' THEN
		BEGIN
			USERS_save;
			WORDS_save;
		END
		ELSE
			IF Mode = 'sort' THEN
			BEGIN
				USERS_sort;
				WORDS_sort;
			END
			ELSE
				IF Mode = 'random' THEN
					WORDS_random;
	IF (Mode = 'inital') OR (Mode = 'backup') THEN 
	BEGIN
		Pcolor := 7;
		Ptype := 'NOTICE';
		IF Mode = 'inital' THEN
			Ptext := 'COMPLETE LOADED!'
		ELSE
			Ptext := 'COMPLETE SAVED!';
		Pkey := TRUE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
	END;
	Mode := '';
END;

FUNCTION USERS_locate(var N : integer) : boolean;
BEGIN
	USERS_locate := FALSE;
	N := 0;
	REPEAT
		N := N + 1;
	UNTIL lowercase(USERS_array[N].Name) = USERS_present;
	USERS_locate := TRUE;
END;

PROCEDURE USERS_ui(var Mode : string);
VAR
	Method : string;
	N : integer;
	ISexist : boolean;

FUNCTION USERS_check(Username, Password : string;var ISexist : boolean) : boolean;
VAR
	N : integer;
BEGIN
	USERS_check := FALSE;
	N := 0;
	REPEAT
		N := N + 1;
		WITH USERS_array[N] DO
			IF (ISexist) AND (lowercase(Name) = lowercase(Username)) AND (Code = Password) THEN
				USERS_check := TRUE
			ELSE
				IF (NOT ISexist) AND (lowercase(Name) = lowercase(Username)) THEN
					USERS_check := TRUE;
	UNTIL (USERS_check) OR (N = USERS_quantity);
	ISexist := FALSE;
END;

FUNCTION USERS_update(var Method : string) : boolean;

PROCEDURE USERS_password;
VAR
	Input : array[1..2] of string;
	Result : boolean;
BEGIN
	REPEAT
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'MAIN MENU > USER SETTING > CHANGE PASSWORD.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		Pcolor := 1;
		Ptype := 'ASKING';
		Ptext := 'OLD PASSWORD?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textcolor(0);
		readln(Input[1]);
		textcolor(7);
		IF length(Input[1]) = 0 THEN
		BEGIN
			Result := FALSE;
			Pcolor := 4;
			Ptype := 'ERROR';
			Ptext := 'EMPTY INPUT!';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
		BEGIN
			ISletter := TRUE;
			ISnumber := TRUE;
			ISroot := FALSE;
			Verify(Input[1], ISletter, ISnumber, ISroot);
			IF NOT Valid THEN
				Result := FALSE
			ELSE
			BEGIN
				Pcolor := 1;
				Ptype := 'ASKING';
				Ptext := 'NEW PASSWORD?';
				Pkey := FALSE;
				Prompt(Pcolor, Ptype, Ptext, Pkey);
				textcolor(0);
				readln(Input[2]);
				textcolor(7);
				IF length(Input[2]) = 0 THEN
				BEGIN
					Result := TRUE;
					Pcolor := 4;
					Ptype := 'NOTICE';
					Ptext := 'PASSWORD HAVE NOT CHANGE!';
					Pkey := TRUE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
				END
				ELSE
				BEGIN
					ISletter := TRUE;
					ISnumber := TRUE;
					ISroot := FALSE;
					Verify(Input[2], ISletter, ISnumber, ISroot);
					IF NOT Valid THEN
						Result := FALSE
					ELSE
					BEGIN
						Input[1] := MD5Print(MD5String(Input[1]));
						USERS_locate(N);
						IF USERS_array[N].Code = Input[1] THEN
						BEGIN
							Input[2] := MD5Print(MD5String(Input[2]));
							USERS_array[N].Code := Input[2];
							Result := TRUE;
							Pcolor := 2;
							Ptype := 'NOTICE';
							Ptext := 'PASSWORD CHANGED!';
							Pkey := TRUE;
							Prompt(Pcolor, Ptype, Ptext, Pkey);
						END
						ELSE
						BEGIN
							Result := FALSE;
							Pcolor := 4;
							Ptype := 'ERROR';
							Ptext := 'OLD PASSWORD INCORRECT!';
							Pkey := TRUE;
							Prompt(Pcolor, Ptype, Ptext, Pkey);
						END;
					END;
				END;
			END;
		END;
	UNTIL Result;
END;

PROCEDURE USERS_identity;
VAR
	Input : string;
BEGIN
	REPEAT
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'MAIN MENU > USER SETTING > CHANGE IDENTITY.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		writeln;
		textbackground(1);
		writeln('>>> PLEASE SELECT <<<');
		textbackground(0);
		writeln('[a] guest - LIMITED');
		writeln('[b] teens - NORMAL');
		writeln('[c] folks - MASTER');
		writeln('[x] GO BACK.');
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'OPTION?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		readln(Input);
		IF length(Input) = 0 THEN
		BEGIN
			Pcolor := 4;
			Ptype := 'ERROR';
			Ptext := 'EMPTY INPUT!';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
		BEGIN
			ISletter := TRUE;
			ISnumber := FALSE;
			ISroot := FALSE;
			Verify(Input[1], ISletter, ISnumber, ISroot);
			IF Valid THEN
			BEGIN
				IF Input[1] IN ['a', 'A', 'b', 'B', 'c', 'C'] THEN
					USERS_locate(N);
				IF Input[1] IN ['a', 'A'] THEN
				BEGIN
					IF USERS_array[N].Identity = 'folks' THEN
					BEGIN
						USERS_array[N].Question := 0;
						USERS_array[N].Correct := 0;
						USERS_array[N].Score := 0;
					END;
					USERS_array[N].Identity := 'guest';
					Pcolor := 2;
					Ptype := 'NOTICE';
					Ptext := 'CHANGED TO "guest"!';
					Pkey := TRUE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
				END
				ELSE
					IF Input[1] IN ['b', 'B'] THEN
					BEGIN
						IF USERS_array[N].Identity = 'folks' THEN
						BEGIN
							USERS_array[N].Question := 0;
							USERS_array[N].Correct := 0;
							USERS_array[N].Score := 0;
						END;
						USERS_array[N].Identity := 'teens';
						Pcolor := 2;
						Ptype := 'NOTICE';
						Ptext := 'CHANGED TO "teens"!';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END
					ELSE
						IF Input[1] IN ['c', 'C'] THEN
						BEGIN
							IF (USERS_array[N].Identity = 'guest') OR (USERS_array[N].Identity = 'teens') THEN
							BEGIN
								USERS_array[N].Question := -1;
								USERS_array[N].Correct := -1;
								USERS_array[N].Score := -1;
							END;
							USERS_array[N].Identity := 'folks';
							Pcolor := 2;
							Ptype := 'NOTICE';
							Ptext := 'CHANGED TO "folks"!';
							Pkey := TRUE;
							Prompt(Pcolor, Ptype, Ptext, Pkey);
						END
						ELSE
							IF Input[1] IN ['x', 'X'] THEN
								clrscr
							ELSE
							BEGIN
								Pcolor := 4;
								Ptype := 'ERROR';
								Ptext := 'UNKNOWN OPTION!';
								Pkey := TRUE;
								Prompt(Pcolor, Ptype, Ptext, Pkey);
							END;
			END;
		END;
	UNTIL Input[1] IN ['a', 'A', 'b', 'B', 'c', 'C', 'x', 'X'];
END;

PROCEDURE USERS_delete;
VAR
	Input : string;
BEGIN
	REPEAT
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'MAIN MENU > USER SETTING > DELETE ACCOUNT.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		Pcolor := 1;
		Ptype := 'ASKING';
		Ptext := 'SURE[y] OR WITHDRAW[n]?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		readln(Input);
		IF length(Input) = 0 THEN
		BEGIN
			Pcolor := 4;
			Ptype := 'ERROR';
			Ptext := 'EMPTY INPUT!';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
		BEGIN
			ISletter := TRUE;
			ISnumber := FALSE;
			ISroot := FALSE;
			Verify(Input[1], ISletter, ISnumber, ISroot);
			IF Valid THEN
			BEGIN
				IF Input[1] IN ['y', 'Y', 'n', 'N'] THEN
					USERS_locate(N);
				IF Input[1] IN ['y', 'Y'] THEN
				BEGIN
					USERS_array[N].Name := '';
					Pcolor := 2;
					Ptype := 'NOTICE';
					Ptext := 'ACCOUNT DELETE SUCCEED!';
					Pkey := TRUE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
				END
				ELSE
					IF Input[1] IN ['n', 'N'] THEN
					BEGIN
						Pcolor := 4;
						Ptype := 'NOTICE';
						Ptext := 'ACCOUNT HAVE NOT DELETE!';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END
					ELSE
					BEGIN
						Pcolor := 4;
						Ptype := 'ERROR';
						Ptext := 'UNKNOWN OPTION!';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END;
			END;
		END;
	UNTIL Input[1] IN ['y', 'Y', 'n', 'N'];
END;

PROCEDURE USERS_reset;
VAR
	Input : string;
BEGIN
	REPEAT
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'MAIN MENU > USER SETTING > RESET ACCOUNT.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		Pcolor := 1;
		Ptype := 'ASKING';
		Ptext := 'SURE[y] OR WITHDRAW[n]?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		readln(Input);
		IF length(Input) = 0 THEN
		BEGIN
			Pcolor := 4;
			Ptype := 'ERROR';
			Ptext := 'EMPTY INPUT!';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
		BEGIN
			ISletter := TRUE;
			ISnumber := FALSE;
			ISroot := FALSE;
			Verify(Input[1], ISletter, ISnumber, ISroot);
			IF Valid THEN
			BEGIN
				IF Input[1] IN ['y', 'Y', 'n', 'N'] THEN
					USERS_locate(N);
				IF Input[1] IN ['y', 'Y'] THEN
				BEGIN
					USERS_array[N].Question := 0;
					USERS_array[N].Correct := 0;
					USERS_array[N].Score := 0;
					Pcolor := 2;
					Ptype := 'NOTICE';
					Ptext := 'ACCOUNT RESET SUCCEED!';
					Pkey := TRUE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
				END
				ELSE
					IF Input[1] IN ['n', 'N'] THEN
					BEGIN
						Pcolor := 4;
						Ptype := 'NOTICE';
						Ptext := 'ACCOUNT HAVE NOT RESET!';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END
					ELSE
					BEGIN
						Pcolor := 4;
						Ptype := 'ERROR';
						Ptext := 'UNKNOWN OPTION!';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END;
			END;
		END;
	UNTIL Input[1] IN ['y', 'Y', 'n', 'N'];
END;

BEGIN
	USERS_update := FALSE;
	IF Method = 'password' THEN
		USERS_password
	ELSE
		IF Method = 'identity' THEN
			USERS_identity
		ELSE
			IF Method = 'delete' THEN
				USERS_delete
			ELSE
				IF Method = 'reset' THEN
					USERS_reset;
	Method := '';
	USERS_update := TRUE;
END;

FUNCTION USERS_signup : boolean;
VAR
	Input : array[1..2] of string;
BEGIN
	USERS_signup := FALSE;
	REPEAT
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'SIGNUP.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textbackground(1);
		writeln('LEAVE BLANK TO GO BACK.');
		textbackground(0);
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'USERNAME?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		readln(Input[1]);
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'PASSWORD?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textcolor(0);
		readln(Input[2]);
		textcolor(7);
		IF (NOT (length(Input[1]) = 0)) AND (length(Input[2]) = 0) THEN
		BEGIN
			Result := TRUE;
			Pcolor := 4;
			Ptype := 'ERROR';
			Ptext := 'EMPTY INPUT!';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
		IF NOT (length(Input[1]) = 0) THEN
			BEGIN
				ISletter := TRUE;
				ISnumber := TRUE;
				ISroot := FALSE;
				Verify(Input[1], ISletter, ISnumber, ISroot);
				IF Valid THEN
				BEGIN
					ISletter := TRUE;
					ISnumber := TRUE;
					ISroot := FALSE;
					Verify(Input[2], ISletter, ISnumber, ISroot);
					IF Valid THEN
					BEGIN
						Input[2] := MD5Print(MD5String(Input[2]));
						ISexist := FALSE;
						Result := USERS_check(Input[1], Input[2], ISexist);
						IF NOT Result THEN
						BEGIN
							USERS_present := Input[1];
							USERS_quantity := USERS_quantity + 1;
							WITH USERS_array[USERS_quantity] DO
							BEGIN
								Name := lowercase(Input[1]);
								Code := Input[2];
								Identity := 'guest';
								Question := 0;
								Correct := 0;
								Score := 0;
							END;
							Pcolor := 2;
							Ptype := 'NOTICE';
							Ptext := 'ACCOUNT CREATED!';
							Pkey := TRUE;
							Prompt(Pcolor, Ptype, Ptext, Pkey);
						END
						ELSE
						BEGIN
							Pcolor := 4;
							Ptype := 'ERROR';
							Ptext := 'USERNAME NOT AVAILABLE!';
							Pkey := TRUE;
							Prompt(Pcolor, Ptype, Ptext, Pkey);
						END;
					END;
				END;
			END;
	UNTIL (length(Input[1]) = 0) OR (NOT Result);
	USERS_signup := TRUE;
END;

FUNCTION USERS_signin(var N : integer) : boolean;
VAR
	Input : array[0..2] of string;
BEGIN
	USERS_signin := FALSE;
	N := 0;
	REPEAT
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'SIGNIN.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textbackground(1);
		writeln('LEAVE BLANK TO GO BACK.');
		textbackground(0);
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'USERNAME?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		readln(Input[1]);
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'PASSWORD?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textcolor(0);
		readln(Input[2]);
		textcolor(7);
		IF (NOT (length(Input[1]) = 0)) AND (length(Input[2]) = 0) THEN
		BEGIN
			Pcolor := 4;
			Ptype := 'ERROR';
			Ptext := 'EMPTY INPUT!';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
			IF NOT (length(Input[1]) = 0) THEN
			BEGIN
				ISletter := TRUE;
				ISnumber := TRUE;
				ISroot := FALSE;
				Verify(Input[1], ISletter, ISnumber, ISroot);
				IF Valid THEN
				BEGIN
					ISletter := TRUE;
					ISnumber := TRUE;
					ISroot := FALSE;
					Verify(Input[2], ISletter, ISnumber, ISroot);
					IF Valid THEN
					BEGIN
						Input[2] := MD5Print(MD5String(Input[2]));
						ISexist := TRUE;
						Result := USERS_check(Input[1], Input[2], ISexist);
						IF Result THEN
						BEGIN
							USERS_present := lowercase(Input[1]);
							Pcolor := 2;
							Ptype := 'NOTICE';
							Ptext := 'ACCESS GRANTED!';
							Pkey := TRUE;
							Prompt(Pcolor, Ptype, Ptext, Pkey);
						END
						ELSE
						BEGIN
							IF NOT (N = USERS_MAXretry) THEN
								N := N + 1;
							Pcolor := 4;
							Ptype := 'ERROR';
							IF N = USERS_MAXretry THEN
								Ptext := 'MAXIUM RETRIES!'
							ELSE
								Ptext := 'USERNAME/ PASSWORD INCORRECT!';
							Pkey := TRUE;
							Prompt(Pcolor, Ptype, Ptext, Pkey);
						END;
					END;
				END;
			END;
	UNTIL (length(Input[1]) = 0) OR ((USERS_present = '') AND (N = USERS_MAXretry)) OR (NOT (USERS_present = ''));
	USERS_signin := TRUE;
END;

FUNCTION USERS_switch : boolean;
VAR
	N : integer;
	Input : string;
BEGIN
	USERS_switch := FALSE;
	REPEAT
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'SIGNUP[y] OR SIGNIN[n]?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textbackground(1);
		writeln('LEAVE BLANK TO SIGNIN.');
		textbackground(0);
		readln(Input);
		IF NOT (length(Input) = 0) THEN
		BEGIN
			ISletter := TRUE;
			ISnumber := FALSE;
			ISroot := FALSE;
			Verify(Input[1], ISletter, ISnumber, ISroot);
		END;
		IF (length(Input) = 0) OR (NOT (length(Input) = 0) AND (Valid)) THEN
		BEGIN
			IF (length(Input) = 0) OR (Input[1] IN ['y', 'Y', 'n', 'N']) THEN
				clrscr;
			IF Input[1] IN ['y', 'Y'] THEN
				USERS_signup
			ELSE
				IF (length(Input) = 0) OR (Input[1] IN ['n', 'N']) THEN
					USERS_signin(N)
				ELSE
				BEGIN
					Pcolor := 4;
					Ptype := 'ERROR';
					Ptext := 'UNKNOWN OPTION!';
					Pkey := TRUE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
				END;
			IF (USERS_present = '') AND ((length(Input) = 0) OR (Input[1] IN ['y', 'Y', 'n', 'N'])) THEN
				clrscr;
		END;
	UNTIL ((N = USERS_MAXretry) OR (NOT (USERS_present = ''))) AND ((length(Input) = 0) OR (Input[1] IN ['y', 'Y', 'n', 'N']));
	USERS_switch := TRUE;
END;

FUNCTION USERS_signout : boolean;
BEGIN
	USERS_signout := FALSE;
	USERS_present := '';
	Pcolor := 2;
	Ptype := 'NOTICE';
	Ptext := 'SIGNOUT SUCCEED!';
	Pkey := TRUE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	USERS_signout := TRUE;
END;

FUNCTION USERS_profile : boolean;
VAR
	N : integer;
	Percent : real;
BEGIN
	USERS_profile := FALSE;
	Pcolor := 7;
	Ptype := 'DOING RIGHT NOW';
	Ptext := 'MAIN MENU > PROFILE.';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	Pcolor := 1;
	Ptype := 'USERNAME';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	writeln(USERS_present);
	USERS_locate(N);
	Pcolor := 1;
	Ptype := 'IDENTITY';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	write(USERS_array[N].Identity, ' - ');
	IF USERS_array[N].Identity = 'guest' THEN
		writeln('LIMITED')
	ELSE
		IF USERS_array[N].Identity = 'teens' THEN
			writeln('NORMAL')
		ELSE
			IF USERS_array[N].Identity = 'folks' THEN
				writeln('MASTER')
			ELSE
				writeln('UNKNOWN');
	Pcolor := 1;
	Ptype := 'QUESTION';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	writeln(USERS_array[N].Question);
	Pcolor := 1;
	Ptype := 'CORRECT';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	writeln(USERS_array[N].Correct);
	Pcolor := 1;
	Ptype := 'ENGLISH STANDARD';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	IF NOT (USERS_array[N].Question = 0) THEN
		Percent := (USERS_array[N].Correct / USERS_array[N].Question) * 100
	ELSE
		Percent := -1;
	CASE round(Percent) OF
		-1 : writeln('NOT AVAILABLE.');
		0..49 : writeln(round(Percent), '%/ TERRIBLE! YOU MUST WORK HARDER AND HARDER!');
		50..79 : writeln(round(Percent), '%/ POOR. NEED IMPROVEMENTS. YOU CAN DO BETTER!');
		80..89 : writeln(round(Percent), '%/ GOOD. HOWEVER YOU SHOULD TRY YOUR BEST.');
		90..100 : writeln(round(Percent), '%/ EXCELLENT! YOU COULD BE THE HIGHEST.');
	END;
	Pcolor := 1;
	Ptype := 'SCORE';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	writeln(USERS_array[N].Score);
	Pcolor := 7;
	Ptype := 'ASKING';
	Ptext := 'GO BACK?';
	Pkey := TRUE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	USERS_profile := TRUE;
END;

FUNCTION USERS_chart : boolean;
VAR
	N, I, R : integer;
	Percent : real;
BEGIN
	USERS_chart := FALSE;
	Pcolor := 7;
	Ptype := 'DOING RIGHT NOW';
	Ptext := 'MAIN MENU > HIGH SCORE.';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	N := 0;
	I := 0;
	REPEAT
		REPEAT
			N := N + 1;
		UNTIL (N = USERS_quantity) OR (USERS_array[N].Identity = 'guest') OR (USERS_array[N].Identity = 'teens');
		IF (USERS_array[N].Identity = 'guest') OR (USERS_array[N].Identity = 'teens') THEN
		BEGIN
			I := I + 1;
			Pcolor := 1;
			Ptype := 'RANKING';
			str(I, Ptext);
			Ptext := 'NUMBER ' + Ptext;
			Pkey := FALSE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
			writeln('USERNAME: ', USERS_array[N].Name);
			textcolor(8);
			FOR R := 1 TO 4 DO
			BEGIN
				CASE R OF
					1 : write('QUESTION: ', USERS_array[N].Question);
					2 : write('CORRECT: ', USERS_array[N].Correct);
					3 :
					BEGIN
						write('ENGLISH STANDARD: ');
						IF NOT (USERS_array[N].Question = 0) THEN
						BEGIN
							Percent := (USERS_array[N].Correct / USERS_array[N].Question) * 100;
							CASE round(Percent) OF
								0..49 : write(round(Percent), '%/ TERRIBLE!');
								50..79 : write(round(Percent), '%/ POOR.');
								80..89 : write(round(Percent), '%/ GOOD.');
								90..100 : write(round(Percent), '%/ EXCELLENT!');
							END;
						END
						ELSE
							write('NOT AVAILABLE.');
					END;
					4 : write('SCORE: ', USERS_array[N].Score);
				END;
				IF R < 4 THEN
					write('/ ')
				ELSE
					writeln;
			END;
			textcolor(7);
		END;
	UNTIL (N = USERS_quantity) OR (I = 4);
	Pcolor := 7;
	Ptype := 'ASKING';
	Ptext := 'GO BACK?';
	Pkey := TRUE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	USERS_chart := TRUE;
END;

FUNCTION USERS_setting : boolean;
VAR
	N : integer;
	Input : string;
BEGIN
	USERS_setting := FALSE;
	REPEAT
		USERS_locate(N);
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'MAIN MENU > USER SETTING.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		writeln;
		textbackground(1);
		writeln('>>> PLEASE SELECT <<<');
		textbackground(0);
		writeln('[a] CHANGE PASSWORD');
		writeln('[b] CHANGE IDENTITY');
		writeln('[c] DELETE ACCOUNT!');
		IF USERS_array[N].Identity = 'teens' THEN
			writeln('[d] RESET ACCOUNT?');
		writeln('[x] GO BACK.');
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'OPTION?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		readln(Input);
		IF length(Input) = 0 THEN
		BEGIN
			Pcolor := 4;
			Ptype := 'ERROR';
			Ptext := 'EMPTY INPUT!';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
		BEGIN
			ISletter := TRUE;
			ISnumber := FALSE;
			ISroot := FALSE;
			Verify(Input[1], ISletter, ISnumber, ISroot);
			IF Valid THEN
				IF Input[1] IN ['a', 'A'] THEN
				BEGIN
					clrscr;
					Method := 'password';
					USERS_update(Method);
				END
				ELSE
					IF Input[1] IN ['b', 'B'] THEN
					BEGIN
						clrscr;
						Method := 'identity';
						USERS_update(Method);
					END
					ELSE
						IF Input[1] IN ['c', 'C'] THEN
						BEGIN
							clrscr;
							Method := 'delete';
							USERS_update(Method);
						END
						ELSE
							IF Input[1] IN ['d', 'D'] THEN
								IF USERS_array[N].Identity = 'teens' THEN
								BEGIN
									clrscr;
									Method := 'reset';
									USERS_update(Method);
								END
								ELSE
								BEGIN
									Pcolor := 4;
									Ptype := 'ERROR';
									Ptext := 'UNKNOWN OPTION!';
									Pkey := TRUE;
									Prompt(Pcolor, Ptype, Ptext, Pkey);
								END
							ELSE
								IF Input[1] IN ['x', 'X'] THEN
								BEGIN
									Mode := 'sort';
									FILES_io(Mode);
								END
								ELSE
								BEGIN
									Pcolor := 4;
									Ptype := 'ERROR';
									Ptext := 'UNKNOWN OPTION!';
									Pkey := TRUE;
									Prompt(Pcolor, Ptype, Ptext, Pkey);
								END;
		END;
	UNTIL Input[1] IN ['x', 'X'];
	USERS_setting := TRUE;
END;

BEGIN
	IF Mode = 'switch' THEN
		USERS_switch
	ELSE
		IF Mode = 'signout' THEN
			USERS_signout
		ELSE
			IF Mode = 'profile' THEN
				USERS_profile
			ELSE
				IF Mode = 'chart' THEN
					USERS_chart
				ELSE
					IF Mode = 'setting' THEN
						USERS_setting;
	Mode := '';
END;

FUNCTION USERS_result : boolean;
VAR
	N, I : integer;
BEGIN
	USERS_result := FALSE;
	USERS_locate(N);
	I := 0;
	REPEAT
		I := I + 1;
		IF (NOT (N = I)) AND (USERS_array[N].Score >= USERS_array[I].Score) THEN
			USERS_result := TRUE;
	UNTIL (USERS_result) OR (I = USERS_quantity);
	IF USERS_result THEN
	BEGIN
		FOR N := 1 TO 5 DO
		BEGIN
			textbackground(1);
			clrscr;
			delay(100);
			textbackground(7);
			clrscr;
			delay(100);
		END;
		textbackground(0);
		clrscr;
		Pcolor := 2;
		Ptype := 'CONGRATULATION';
		Ptext := 'YOU GOT HIGHEST SCORE!';
		Pkey := TRUE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
	END
	ELSE
		IF I = USERS_quantity THEN
		BEGIN
			textbackground(1);
			clrscr;
			delay(500);
			textbackground(7);
			clrscr;
			delay(500);
			textbackground(0);
			clrscr;
			Pcolor := 4;
			Ptype := 'SORRY & PITY';
			Ptext := 'YOU ARE NOT GOOD ENOUGH!';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END;
END;

FUNCTION USERS_question(var Mode : string) : boolean;
VAR
	N : integer;
BEGIN
	USERS_question := FALSE;
	USERS_locate(N);
	IF Mode = 'increase' THEN
		USERS_array[N].Question := USERS_array[N].Question + 1
	ELSE
		IF Mode = 'decrease' THEN
			USERS_array[N].Question := USERS_array[N].Question - 1;
	Mode := '';
	USERS_question := TRUE;
END;

FUNCTION USERS_correct(var Mode : string) : boolean;
VAR
	N : integer;
BEGIN
	USERS_correct := FALSE;
	USERS_locate(N);
	IF Mode = 'increase' THEN
		USERS_array[N].Correct := USERS_array[N].Correct + 1
	ELSE
		IF Mode = 'decrease' THEN
			USERS_array[N].Correct := USERS_array[N].Correct - 1;
	Mode := '';
	USERS_correct := TRUE;
END;

FUNCTION USERS_score(var Mode, Method : string) : boolean;
VAR
	N, I : integer;
BEGIN
	USERS_score := FALSE;
	USERS_locate(N);
	val(Method, I, Useless);
	IF Mode = 'increase' THEN
		USERS_array[N].Score := USERS_array[N].Score + I
	ELSE
		IF Mode = 'decrease' THEN
			USERS_array[N].Score := USERS_array[N].Score - I;
	Mode := '';
	Method := '';
	USERS_score := TRUE;
END;

PROCEDURE WORDS_ui(var Mode : string);
VAR
	Method, Input : string;

FUNCTION WORDS_check(Vocab : string) : boolean;
VAR
	N : integer;
BEGIN
	WORDS_check := FALSE;
	N := 0;
	REPEAT
		N := N + 1;
		IF lowercase(WORDS_array[N].Vocab) = lowercase(Vocab) THEN
			WORDS_check := TRUE;
	UNTIL (WORDS_check) OR (N = WORDS_quantity);
END;

FUNCTION WORDS_update(var Method : string) : boolean;

PROCEDURE WORDS_insert;
VAR
	Input : array[1..3] of string;
	Result : boolean;
BEGIN
	REPEAT
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'MAIN MENU > DICT. SETTING > INSERT VOCAB.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textbackground(1);
		writeln('LEAVE BLANK TO GO BACK/ RESET.');
		textbackground(0);
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'VOCAB?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		readln(Input[1]);
		IF length(Input[1]) = 0 THEN
			clrscr
		ELSE
		BEGIN
			ISletter := TRUE;
			ISnumber := FALSE;
			ISroot := FALSE;
			Verify(Input[1], ISletter, ISnumber, ISroot);
			IF Valid THEN
			BEGIN
				Result := WORDS_check(Input[1]);
				IF NOT Result THEN
				BEGIN
					Pcolor := 7;
					Ptype := 'ASKING';
					Ptext := 'GARDE~ EASY[1]/ NORMAL[2]/ DIFFICULT[3]/ INSANE[4]?';
					Pkey := FALSE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
					readln(Input[2]);
					Pcolor := 7;
					Ptype := 'ASKING';
					Ptext := 'PART~ NOUN[1]/ VERB[2]/ ADJECTIVE[3]/ ADVERB[4]/ OTHERS[5]?';
					Pkey := FALSE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
					readln(Input[3]);
					IF (length(Input[2]) = 0) OR (length(Input[3]) = 0) THEN
					BEGIN
						Pcolor := 4;
						Ptype := 'ERROR';
						Ptext := 'EMPTY INPUT!';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END
					ELSE
					BEGIN
						ISletter := FALSE;
						ISnumber := TRUE;
						ISroot := FALSE;
						Verify(Input[2][1], ISletter, ISnumber, ISroot);
						IF Valid THEN
							IF Input[2][1] IN ['1'..'4'] THEN
							BEGIN
								ISletter := FALSE;
								ISnumber := TRUE;
								ISroot := FALSE;
								Verify(Input[3][1], ISletter, ISnumber, ISroot);
								IF Valid THEN
									IF Input[3][1] IN ['1'..'5'] THEN
									BEGIN
										WORDS_quantity := WORDS_quantity + 1;
										WITH WORDS_array[WORDS_quantity] DO
										BEGIN
											val(Input[2][1], Grade, Useless);
											val(Input[3][1], Part, Useless);
											Vocab := Input[1];
											Used := FALSE;
										END;
										Pcolor := 2;
										Ptype := 'NOTICE';
										Ptext := 'VOCAB INSERTED!';
										Pkey := TRUE;
										Prompt(Pcolor, Ptype, Ptext, Pkey);
									END
									ELSE
									BEGIN
										Pcolor := 4;
										Ptype := 'ERROR';
										Ptext := 'UNKNOWN OPTION!';
										Pkey := TRUE;
										Prompt(Pcolor, Ptype, Ptext, Pkey);
									END;
							END
							ELSE
							BEGIN
								Pcolor := 4;
								Ptype := 'ERROR';
								Ptext := 'UNKNOWN OPTION!';
								Pkey := TRUE;
								Prompt(Pcolor, Ptype, Ptext, Pkey);
							END;
					END;
				END
				ELSE
				BEGIN
					Pcolor := 4;
					Ptype := 'ERROR';
					Ptext := 'VOCAB EXISTS!';
					Pkey := TRUE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
				END;
			END;
		END;
	UNTIL length(Input[1]) = 0;
END;

PROCEDURE WORDS_remove;
VAR
	N : integer;
	Input : array[1..2] of string;
	Result : boolean;
BEGIN
	REPEAT
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'MAIN MENU > DICT. SETTING > REMOVE VOCAB.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textbackground(1);
		writeln('LEAVE BLANK TO GO BACK.');
		textbackground(0);
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'VOCAB?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		readln(Input[1]);
		IF length(Input[1]) = 0 THEN
			clrscr
		ELSE
		BEGIN
			ISletter := TRUE;
			ISnumber := FALSE;
			ISroot := FALSE;
			Verify(Input[1][1], ISletter, ISnumber, ISroot);
			IF Valid THEN
			BEGIN
				Result := WORDS_check(Input[1]);
				IF Result THEN
				BEGIN
					Pcolor := 7;
					Ptype := 'ASKING';
					Ptext := 'SURE[y] OR WITHDRAW[n]?';
					Pkey := FALSE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
					readln(Input[2]);
					IF length(Input[2]) = 0 THEN
					BEGIN
						Pcolor := 4;
						Ptype := 'ERROR';
						Ptext := 'EMPTY INPUT!';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END
					ELSE
					BEGIN
						ISletter := TRUE;
						ISnumber := FALSE;
						ISroot := FALSE;
						Verify(Input[2][1], ISletter, ISnumber, ISroot);
						IF Valid THEN
							IF Input[2][1] IN ['y', 'Y'] THEN
							BEGIN
								N := 0;
								REPEAT
									N := N + 1;
								UNTIL (lowercase(WORDS_array[N].Vocab) = lowercase(Input[1]));
								WORDS_array[N].Vocab := '';
								WORDS_array[N].Used := FALSE;
								Pcolor := 2;
								Ptype := 'NOTICE';
								Ptext := 'VOCAB REMOVED!';
								Pkey := TRUE;
								Prompt(Pcolor, Ptype, Ptext, Pkey);
							END
							ELSE
								IF Input[2][1] IN ['n', 'N'] THEN
								BEGIN
									Pcolor := 4;
									Ptype := 'NOTICE';
									Ptext := 'VOCAB HAVE NOT REMOVE!';
									Pkey := TRUE;
									Prompt(Pcolor, Ptype, Ptext, Pkey);
								END
								ELSE
								BEGIN
									Pcolor := 4;
									Ptype := 'ERROR';
									Ptext := 'UNKNOWN OPTION!';
									Pkey := TRUE;
									Prompt(Pcolor, Ptype, Ptext, Pkey);
								END;
					END;
				END
				ELSE
				BEGIN
					Pcolor := 4;
					Ptype := 'ERROR';
					Ptext := 'VOCAB NOT EXIST!';
					Pkey := TRUE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
				END;
			END;
		END;
	UNTIL (length(Input[1]) = 0) OR (Result);
END;

BEGIN
	WORDS_update := FALSE;
	IF Method = 'insert' THEN
		WORDS_insert
	ELSE
		IF Method = 'remove' THEN
			WORDS_remove;
	Method := '';
	WORDS_update := TRUE;
END;

FUNCTION WORDS_rules : boolean;
BEGIN
	WORDS_rules := FALSE;
	clrscr;
	Pcolor := 7;
	Ptype := 'DOING RIGHT NOW';
	Ptext := 'MAIN MENU > PLAY NOW > RULES.';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	Pcolor := 1;
	Ptype := 'GENERAL';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	writeln('1] TRY YOUR BEST TO REACH YOUR BEST RESULT!');
	writeln('2] FINISH BY YOURSELF AND DO NOT CHEAT!');
	writeln('3] MARK DOWN IF YOU DON''T KNOW THE WORD.');
	writeln('4] THINK TWICE BEFORE GIVING UP THIS ROUND.');
	writeln('5] HARD-WROKING IS MORE IMPORTANT THAN SCORE.');
	Pcolor := 1;
	Ptype := 'SCORE';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	writeln('a] CORRECT GET 30 POINTS.');
	writeln('b] NO POINTS WILL DEDUCT IF YOU TRIED ', WORDS_MAXretry, ' TIMES.');
	writeln('c] PASS DEDUCT 10 POINTS.');
	writeln('d] NO POINTS WILL DEDUCT IF YOU EXIT.');
	Pcolor := 7;
	Ptype := 'ASKING';
	Ptext := 'ARE YOU READY?';
	Pkey := TRUE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	WORDS_rules := TRUE;
END;

FUNCTION WORDS_reorder(Level : integer; var Question : integer) : boolean;
TYPE
	Htype = RECORD
							 Letter : string[1];
							 Correct : boolean;
							 OOrder : integer;
							 NOrder : integer;
						 END;
VAR
	Harray : array[1..WORDS_MAXspan] of Htype;
	Retry : integer;
	Vocab : string[WORDS_MAXspan];
	Input : string;
	Result : boolean;

PROCEDURE WORDS_hint(var Method : string;Vocab, Input : string);

FUNCTION Hsetup : boolean;
VAR
	N, I : integer;
BEGIN
	Hsetup := FALSE;
	FOR N := 1 TO length(Vocab) DO
		WITH Harray[N] DO
		BEGIN
			Letter := Vocab[N];
			Correct := FALSE;
			OOrder := N;
			NOrder := N;
		END;
	REPEAT
		FOR N := 1 TO length(Vocab) DO
		BEGIN
			Haphazard := random(length(Vocab)) + 1;
			Useless := Harray[Haphazard].NOrder;
			Harray[Haphazard].NOrder := Harray[N].NOrder;
			Harray[N].NOrder := Useless;
		END;
		I := 0;
		FOR N := 1 TO length(Vocab) DO
			WITH Harray[N] DO
				IF OOrder = NOrder THEN
					I := I + 1;
	UNTIL I < length(Vocab);
	Hsetup := TRUE;
END;

FUNCTION Hleft : boolean;
VAR
	N : integer;
BEGIN
	Hleft := FALSE;
	Pcolor := 1;
	Ptype := 'LEFT BEHIND';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	FOR N := 1 TO length(Vocab) DO
		IF NOT Harray[Harray[N].NOrder].Correct THEN
		BEGIN
			write(Harray[Harray[N].NOrder].Letter);
			IF N < length(Vocab) THEN
			BEGIN
				textcolor(8);
				write('|');
				textcolor(7);
			END;
		END;
	writeln;
	Hleft := TRUE;
END;

FUNCTION Hlast : boolean;
VAR
	N : integer;
BEGIN
	Hlast := FALSE;
	Pcolor := 1;
	Ptype := 'LAST ATTEMPT';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	IF Valid THEN
	BEGIN
		FOR N := 1 TO length(Input) DO
		BEGIN
			IF Harray[N].Correct THEN
			BEGIN
				textcolor(10);
				write(Harray[N].Letter);
				textcolor(7);
			END
			ELSE
				write(Input[N]);
			IF N < length(Input) THEN
			BEGIN
				textcolor(8);
				write('|');
				textcolor(7);
			END;
		END;
		writeln;
	END
	ELSE
		writeln('NOT AVAILABLE');
	Hlast := TRUE;
END;

FUNCTION Hdetail : boolean;
VAR
	N : integer;
BEGIN
	N := 0;
	REPEAT
		N := N + 1;
	UNTIL WORDS_array[N].Vocab = Vocab;
	Hdetail := FALSE;
	Pcolor := 1;
	Ptype := 'DETAIL';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	CASE WORDS_array[N].Grade OF
		1 : write('EASY');
		2 : write('NORMAL');
		3 : write('DIFFICULT');
	END;
	write(' - ');
	CASE WORDS_array[N].Part OF
		1 : write('NOUN');
		2 : write('VERB');
		3 : write('ADJECTIVE');
		4 : write('ADVERB');
		5 : write('OTHERS');
	END;
	write(' - BEGIN WITH: ');
	CASE length(Vocab) OF
		3..5 : write(WORDS_array[N].Vocab[1]);
		6..WORDS_MAXspan : write(WORDS_array[N].Vocab[1], WORDS_array[N].Vocab[2]);
	END;
	writeln;
	Hdetail := TRUE;
END;

FUNCTION Hcheck : boolean;
VAR
	N, I : integer;
BEGIN
	Hcheck := FALSE;
	I := 0;
	FOR N := 1 TO length(Vocab) DO
		IF Harray[N].Correct THEN
			I := I + 1
		ELSE
			IF (Input[N] = Harray[N].Letter) AND (Harray[N].OOrder = N) THEN
			BEGIN
				Harray[N].Correct := TRUE;
				I := I + 1;
			END;
	IF (I = length(Vocab)) AND (length(Input) = length(Vocab)) THEN
		Hcheck := TRUE;
END;

BEGIN
	IF Method = 'setup' THEN
		Hsetup
	ELSE
		IF Method = 'left' THEN
			Hleft
		ELSE
			IF Method = 'detail' THEN
				Hdetail
			ELSE
				IF Method = 'last' THEN
					Hlast
				ELSE
					IF Method = 'check' THEN
						Result := Hcheck;
END;

BEGIN
	WORDS_reorder := FALSE;
	Retry := 0;
	Input := '';
	Vocab := '';
	REPEAT
		IF Retry = WORDS_MAXretry THEN
		BEGIN
			Retry := 0;
			Input := '';
		END;
		IF (Retry = 0) AND (Input = '') THEN
		BEGIN
			REPEAT
				Haphazard := random(WORDS_quantity) + 1;
			UNTIL (WORDS_array[Haphazard].Grade = Level) AND (NOT WORDS_array[Haphazard].Used) AND (NOT (Vocab = WORDS_array[Haphazard].Vocab));
			WORDS_array[Haphazard].Used := TRUE;
			Vocab := WORDS_array[Haphazard].Vocab;
		END;
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'MAIN MENU > PLAY NOW > REORDER THE LETTERS.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textbackground(1);
		writeln('THIS ROUND HAS ', Question, ' MORE QUESTIONS TO GO.');
		textbackground(0);
		Method := 'detail';
		WORDS_hint(Method, Vocab, Input);
		IF (Retry = 0) AND (Input = '') THEN
		BEGIN
			Method := 'setup';
			WORDS_hint(Method, Vocab, Input);
		END;
		Method := 'left';
		WORDS_hint(Method, Vocab, Input);
		IF NOT (Retry = 0) THEN
		BEGIN
			Method := 'last';
			WORDS_hint(Method, Vocab, Input);
		END;
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'ANSWER?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textbackground(1);
		writeln('LEAVE BLANK TO PASS/ ENTER NUMBER TO EXIT.');
		textbackground(0);
		readln(Input);
		IF length(Input) = 0 THEN
		BEGIN
			Question := Question - 1;
			Mode := 'increase';
			USERS_question(Mode);
			Useless := 10;
			Mode := 'decrease';
			str(Useless, Method);
			USERS_score(Mode, Method);
			Retry := 0;
			Pcolor := 7;
			Ptype := 'CORRECT SPELLING';
			Ptext := Vocab;
			Pkey := FALSE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
			Pcolor := 4;
			Ptype := 'NOTICE';
			Ptext := 'THIS QUESTION SKIPPED.';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
			IF Input[1] IN ['0'..'9'] THEN 
			BEGIN
				Question := 0;
				Useless := 10;
				Mode := 'decrease';
				str(Useless, Method);
				USERS_score(Mode, Method);
				Retry := WORDS_MAXretry;
				Pcolor := 4;
				Ptype := 'NOTICE';
				Ptext := 'EXITING THE GAME.';
				Pkey := TRUE;
				Prompt(Pcolor, Ptype, Ptext, Pkey);
			END
			ELSE
			BEGIN
				ISletter := TRUE;
				ISnumber := FALSE;
				ISroot := FALSE;
				Verify(Input, ISletter, ISnumber, ISroot);
				IF Valid THEN
				BEGIN
					Method := 'check';
					WORDS_hint(Method, Vocab, Input);
					IF Result THEN
					BEGIN
						Question := Question - 1;
						Mode := 'increase';
						USERS_question(Mode);
						Mode := 'increase';
						USERS_correct(Mode);
						Useless := 30;
						Mode := 'increase';
						str(Useless, Method);
						USERS_score(Mode, Method);
						Retry := WORDS_MAXretry;
						Pcolor := 2;
						Ptype := 'NOTICE';
						Ptext := 'RIGHT! GOING TO NEXT ONE.';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END
					ELSE
					BEGIN
						IF Retry < WORDS_MAXretry THEN
							Retry := Retry + 1;
						IF Retry = WORDS_MAXretry THEN
						BEGIN
							Mode := 'increase';
							USERS_question(Mode);
							Pcolor := 7;
							Ptype := 'CORRECT SPELLING';
							Ptext := Vocab;
							Pkey := FALSE;
							Prompt(Pcolor, Ptype, Ptext, Pkey);
						END;
						Pcolor := 4;
						Ptype := 'NOTICE';
						IF Retry < WORDS_MAXretry THEN
							Ptext := 'WRONG! TRY HARDER.'
						ELSE
							Ptext := 'MAXIUM RETRIES.';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END;
				END;
			END;
	UNTIL (Input[1] IN ['0'..'9']) OR (Question = 0);
	WORDS_reorder := TRUE;
END;

FUNCTION WORDS_fill(Level : integer; var Question : integer) : boolean;
TYPE
	Htype = RECORD
							 Letter : string[1];
							 Correct : boolean;
						 END;
VAR
	Harray : array[1..WORDS_MAXspan] of Htype;
	Retry : integer;
	Vocab : string[WORDS_MAXspan];
	Input : string;
	Result : boolean;

PROCEDURE WORDS_hint(var Method : string;Vocab, Input : string);

FUNCTION Hsetup : boolean;
VAR
	N : integer;
BEGIN
	Hsetup := FALSE;
	FOR N := 1 TO length(Vocab) DO
		WITH Harray[N] DO
		BEGIN
			Letter := Vocab[N];
			Correct := FALSE;
		END;
	FOR N := 1 TO (length(Vocab) - length(Vocab) div 3) DO
	BEGIN
		Haphazard := random(length(Vocab)) + 1;
		Harray[Haphazard].Correct := TRUE;
	END;;
	Hsetup := TRUE;
END;

FUNCTION Hleft : boolean;
VAR
	N : integer;
BEGIN
	Hleft := FALSE;
	Pcolor := 1;
	Ptype := 'LEFT BEHIND';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	FOR N := 1 TO length(Vocab) DO
	BEGIN
		IF Harray[N].Correct THEN
			write(Harray[N].Letter)
		ELSE
			write('*');
		IF N < length(Vocab) THEN
		BEGIN
			textcolor(8);
			write('|');
			textcolor(7);
		END;
	END;
	writeln;
	Hleft := TRUE;
END;

FUNCTION Hlast : boolean;
VAR
	N : integer;
BEGIN
	Hlast := FALSE;
	Pcolor := 1;
	Ptype := 'LAST ATTEMPT';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	IF Valid THEN
	BEGIN
		FOR N := 1 TO length(Input) DO
		BEGIN
			IF Harray[N].Correct THEN
			BEGIN
				textcolor(10);
				write(Harray[N].Letter);
				textcolor(7);
			END
			ELSE
				write(Input[N]);
			IF N < length(Input) THEN
			BEGIN
				textcolor(8);
				write('|');
				textcolor(7);
			END;
		END;
		writeln;
	END
	ELSE
		writeln('NOT AVAILABLE');
	Hlast := TRUE;
END;

FUNCTION Hdetail : boolean;
VAR
	N : integer;
BEGIN
	N := 0;
	REPEAT
		N := N + 1;
	UNTIL WORDS_array[N].Vocab = Vocab;
	Hdetail := FALSE;
	Pcolor := 1;
	Ptype := 'DETAIL';
	Ptext := '';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	CASE WORDS_array[N].Grade OF
		1 : write('EASY');
		2 : write('NORMAL');
		3 : write('DIFFICULT');
	END;
	write(' - ');
	CASE WORDS_array[N].Part OF
		1 : write('NOUN');
		2 : write('VERB');
		3 : write('ADJECTIVE');
		4 : write('ADVERB');
		5 : write('OTHERS');
	END;
	write(' - LENGTH: ', length(Vocab));
	writeln;
	Hdetail := TRUE;
END;

FUNCTION Hcheck : boolean;
VAR
	N, I : integer;
BEGIN
	Hcheck := FALSE;
	I := 0;
	FOR N := 1 TO length(Vocab) DO
		IF Harray[N].Correct THEN
			I := I + 1
		ELSE
			IF Input[N] = Harray[N].Letter THEN
			BEGIN
				Harray[N].Correct := TRUE;
				I := I + 1;
			END;
	IF (I = length(Vocab)) AND (length(Input) = length(Vocab)) THEN
		Hcheck := TRUE;
END;

BEGIN
	IF Method = 'setup' THEN
		Hsetup
	ELSE
		IF Method = 'left' THEN
			Hleft
		ELSE
			IF Method = 'detail' THEN
				Hdetail
			ELSE
				IF Method = 'last' THEN
					Hlast
				ELSE
					IF Method = 'check' THEN
						Result := Hcheck;
END;

BEGIN
	WORDS_fill := FALSE;
	Retry := 0;
	Input := '';
	Vocab := '';
	REPEAT
		IF Retry = WORDS_MAXretry THEN
		BEGIN
			Retry := 0;
			Input := '';
		END;
		IF (Retry = 0) AND (Input = '') THEN
		BEGIN
			REPEAT
				Haphazard := random(WORDS_quantity) + 1;
			UNTIL (WORDS_array[Haphazard].Grade = Level) AND (NOT WORDS_array[Haphazard].Used) AND (NOT (Vocab = WORDS_array[Haphazard].Vocab));
			WORDS_array[Haphazard].Used := TRUE;
			Vocab := WORDS_array[Haphazard].Vocab;
		END;
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'MAIN MENU > PLAY NOW > FILLING THE BLANKS.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textbackground(1);
		writeln('THIS ROUND HAS ', Question, ' MORE QUESTIONS TO GO.');
		textbackground(0);
		Method := 'detail';
		WORDS_hint(Method, Vocab, Input);
		IF (Retry = 0) AND (Input = '') THEN
		BEGIN
			Method := 'setup';
			WORDS_hint(Method, Vocab, Input);
		END;
		Method := 'left';
		WORDS_hint(Method, Vocab, Input);
		IF NOT (Retry = 0) THEN
		BEGIN
			Method := 'last';
			WORDS_hint(Method, Vocab, Input);
		END;
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'ANSWER?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		textbackground(1);
		writeln('LEAVE BLANK TO PASS/ ENTER NUMBER TO EXIT.');
		textbackground(0);
		readln(Input);
		IF length(Input) = 0 THEN
		BEGIN
			Question := Question - 1;
			Mode := 'increase';
			USERS_question(Mode);
			Useless := 10;
			Mode := 'decrease';
			str(Useless, Method);
			USERS_score(Mode, Method);
			Retry := 0;
			Pcolor := 7;
			Ptype := 'CORRECT SPELLING';
			Ptext := Vocab;
			Pkey := FALSE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
			Pcolor := 4;
			Ptype := 'NOTICE';
			Ptext := 'THIS QUESTION SKIPPED.';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
			IF Input[1] IN ['0'..'9'] THEN 
			BEGIN
				Question := 0;
				Useless := 10;
				Mode := 'decrease';
				str(Useless, Method);
				USERS_score(Mode, Method);
				Retry := WORDS_MAXretry;
				Pcolor := 4;
				Ptype := 'NOTICE';
				Ptext := 'EXITING THE GAME.';
				Pkey := TRUE;
				Prompt(Pcolor, Ptype, Ptext, Pkey);
			END
			ELSE
			BEGIN
				ISletter := TRUE;
				ISnumber := FALSE;
				ISroot := FALSE;
				Verify(Input, ISletter, ISnumber, ISroot);
				IF Valid THEN
				BEGIN
					Method := 'check';
					WORDS_hint(Method, Vocab, Input);
					IF Result THEN
					BEGIN
						Question := Question - 1;
						Mode := 'increase';
						USERS_question(Mode);
						Mode := 'increase';
						USERS_correct(Mode);
						Useless := 30;
						Mode := 'increase';
						str(Useless, Method);
						USERS_score(Mode, Method);
						Retry := WORDS_MAXretry;
						Pcolor := 2;
						Ptype := 'NOTICE';
						Ptext := 'RIGHT! GOING TO NEXT ONE.';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END
					ELSE
					BEGIN
						IF Retry < WORDS_MAXretry THEN
							Retry := Retry + 1;
						IF Retry = WORDS_MAXretry THEN
						BEGIN
							Mode := 'increase';
							USERS_question(Mode);
							Pcolor := 7;
							Ptype := 'CORRECT SPELLING';
							Ptext := Vocab;
							Pkey := FALSE;
							Prompt(Pcolor, Ptype, Ptext, Pkey);
						END;
						Pcolor := 4;
						Ptype := 'NOTICE';
						IF Retry < WORDS_MAXretry THEN
							Ptext := 'WRONG! TRY HARDER.'
						ELSE
							Ptext := 'MAXIUM RETRIES.';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END;
				END;
			END;
	UNTIL (Input[1] IN ['0'..'9']) OR (Question = 0);
	WORDS_fill := TRUE;
END;

FUNCTION WORDS_switch : boolean;
VAR
	N, I, Level, Question : integer;
	Input : array[1..3] of string;
	Rubbish : array[1..3] of string;
BEGIN
	WORDS_switch := FALSE;
	REPEAT
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'MAIN MENU > PLAY NOW > PREPARTAION.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		writeln;
		textbackground(1);
		writeln('>>> PLEASE SELECT <<<');
		textbackground(0);
		writeln('[a] REORDER THE LETTERS');
		writeln('[b] FILLING THE BLANKS');
		writeln('[x] GO BACK.');
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'OPTION?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		readln(Input[1]);
		IF length(Input[1]) = 0 THEN
		BEGIN
			Pcolor := 4;
			Ptype := 'ERROR';
			Ptext := 'EMPTY INPUT!';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
		BEGIN
			ISletter := TRUE;
			ISnumber := FALSE;
			ISroot := FALSE;
			Verify(Input[1][1], ISletter, ISnumber, ISroot);
			IF Valid THEN
				IF Input[1][1] IN ['a', 'A', 'b', 'B'] THEN
				BEGIN
					Pcolor := 7;
					Ptype := 'ASKING';
					Ptext := 'LEVEL~ EASY[1]/ NORMAL[2]/ DIFFICULT[3]/ INSANE[4]?';
					Pkey := FALSE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
					readln(Input[2]);
					Pcolor := 7;
					Ptype := 'ASKING';
					str(WORDS_MINquestion, Rubbish[1]);
					str(WORDS_MAXquestion, Rubbish[2]);
					Ptext := 'QUESTIONS? [FROM ' + Rubbish[1] + ' TO ' + Rubbish[2] + ']';
					Pkey := FALSE;
					Prompt(Pcolor, Ptype, Ptext, Pkey);
					textbackground(1);
					writeln('LEAVE BLANK TO RANDOM.');
					textbackground(0);
					readln(Input[3]);
					IF length(Input[2]) = 0 THEN
					BEGIN
						Pcolor := 4;
						Ptype := 'ERROR';
						Ptext := 'EMPTY INPUT!';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END
					ELSE
					BEGIN
						ISletter := FALSE;
						ISnumber := TRUE;
						ISroot := FALSE;
						Verify(Input[2], ISletter, ISnumber, ISroot);
						IF Valid THEN
						BEGIN
							val(Input[2], Level, Useless);
							IF NOT (Level IN [1..4]) THEN
							BEGIN
								Pcolor := 4;
								Ptype := 'ERROR';
								Ptext := 'UNKNOWN OPTION!';
								Pkey := TRUE;
								Prompt(Pcolor, Ptype, Ptext, Pkey);
							END
							ELSE
							BEGIN
								I := 0;
								FOR N := 1 TO WORDS_quantity DO
									IF WORDS_array[N].Grade = Level THEN
										I := I + 1;
								IF I < WORDS_MAXquestion THEN
								BEGIN
									I := -1;
									Pcolor := 4;
									Ptype := 'ERROR';
									Ptext := 'VOCAB IN THIS LEVEL NOT ENOUGH!';
									Pkey := TRUE;
									Prompt(Pcolor, Ptype, Ptext, Pkey);
								END
								ELSE
									IF length(Input[3]) = 0 THEN
									BEGIN
										REPEAT
											Haphazard := random(WORDS_MAXquestion) + 1;
										UNTIL Haphazard >= WORDS_MINquestion;
										Question := Haphazard;
										Pcolor := 2;
										Ptype := 'NOTICE';
										str(Question, Rubbish[3]);
										Ptext := Rubbish[3] + ' QUESTIONS!';
										Pkey := TRUE;
										Prompt(Pcolor, Ptype, Ptext, Pkey);
									END
									ELSE
									BEGIN
										ISletter := FALSE;
										ISnumber := TRUE;
										ISroot := FALSE;
										Verify(Input[3], ISletter, ISnumber, ISroot);
										IF Valid THEN
										BEGIN
											val(Input[3], Question, Useless);
											IF NOT (Question IN [WORDS_MINquestion..WORDS_MAXquestion]) THEN
											BEGIN
												Pcolor := 4;
												Ptype := 'ERROR';
												Ptext := 'NOT IN THE ALLOWED RANGE!';
												Pkey := TRUE;
												Prompt(Pcolor, Ptype, Ptext, Pkey);
											END
											ELSE
												clrscr;
										END;
									END;
							END;
						END;
					END;
				END
				ELSE
					IF NOT (Input[1][1] IN ['x', 'X']) THEN
					BEGIN
						Pcolor := 4;
						Ptype := 'ERROR';
						Ptext := 'UNKNOWN OPTION!';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END;
		END;
	UNTIL ((Input[1][1] IN ['a', 'A', 'b', 'B']) AND (Level IN [1..4]) AND (NOT (I = -1)) AND (I >= WORDS_MAXquestion) AND (Question IN [WORDS_MINquestion..WORDS_MAXquestion])) OR (Input[1][1] IN ['x', 'X']);
	IF Input[1][1] IN ['a', 'A', 'b', 'B'] THEN
	BEGIN
		WORDS_rules;
		IF Input[1][1] IN ['a', 'A'] THEN
			WORDS_reorder(Level, Question)
		ELSE
			IF Input[1][1] IN ['b', 'B'] THEN
				WORDS_fill(Level, Question);
		USERS_result;
	END
	ELSE
		IF Input[1][1] IN ['x', 'X'] THEN
			clrscr;
	WORDS_switch := TRUE;
END;

FUNCTION WORDS_chart : boolean;
VAR
	N, I, R, L : integer;
BEGIN
	WORDS_chart := FALSE;
	Pcolor := 7;
	Ptype := 'DOING RIGHT NOW';
	Ptext := 'MAIN MENU > DICT. SETTING > STATS.';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	Pcolor := 1;
	Ptype := 'GENERAL';
	Ptext := 'ALL GRADES.';
	Pkey := FALSE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	N := 0;
	I := 0;
	REPEAT
		N := N + 1;
		IF NOT (WORDS_array[N].Vocab = '') THEN
			I := I + 1;
	UNTIL N = WORDS_quantity;
	writeln('TOTAL: ', I);
	FOR N := 1 TO 4 DO
	BEGIN
		Pcolor := 1;
		Ptype := 'GRADE';
		CASE N OF
			1 : Ptext := 'EASY';
			2 : Ptext := 'NORMAL';
			3 : Ptext := 'DIFFICULT';
			4 : Ptext := 'INSANE';
		END;
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		I := 0;
		R := 0;
		REPEAT
			I := I + 1;
			IF (NOT (WORDS_array[I].Vocab = '')) AND (WORDS_array[I].Grade = N) THEN
				R := R + 1;
		UNTIL I = WORDS_quantity;
		writeln('TOTAL: ', R);
		textcolor(8);
		FOR I := 1 TO 4 DO
		BEGIN
			CASE I OF
				1 : write('NOUN: ');
				2 : write('VERB: ');
				3 : write('ADJECTIVE: ');
				4 : write('ADVERB: ');
				5 : write('OTHERS: ');
			END;
			R := 0;
			L := 0;
			REPEAT
				R := R + 1;
				IF (NOT (WORDS_array[R].Vocab = '')) AND (WORDS_array[R].Grade = N) AND (WORDS_array[R].Part = I) THEN
					L := L + 1;
			UNTIL R = WORDS_quantity;
			write(L);
			IF I < 4 THEN
				write('/ ')
			ELSE
				writeln;
		END;
		textcolor(7);
	END;
	Pcolor := 7;
	Ptype := 'ASKING';
	Ptext := 'GO BACK?';
	Pkey := TRUE;
	Prompt(Pcolor, Ptype, Ptext, Pkey);
	WORDS_chart := TRUE;
END;

FUNCTION WORDS_setting : boolean;
BEGIN
	WORDS_setting := FALSE;
	REPEAT
		Pcolor := 7;
		Ptype := 'DOING RIGHT NOW';
		Ptext := 'MAIN MENU > DICT. SETTING.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		writeln;
		textbackground(1);
		writeln('>>> PLEASE SELECT <<<');
		textbackground(0);
		writeln('[a] STATS');
		writeln('[b] INSERT VOCAB');
		writeln('[c] REMOVE VOCAB');
		writeln('[x] GO BACK.');
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'OPTION?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		readln(Input);
		IF length(Input) = 0 THEN
		BEGIN
			Pcolor := 4;
			Ptype := 'ERROR';
			Ptext := 'EMPTY INPUT!';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
		BEGIN
			ISletter := TRUE;
			ISnumber := FALSE;
			ISroot := FALSE;
			Verify(Input[1], ISletter, ISnumber, ISroot);
			IF Valid THEN
			BEGIN
				IF Input[1] IN ['a', 'A', 'b', 'B', 'c', 'C', 'x', 'X'] THEN
					clrscr;
				IF Input[1] IN ['a', 'A'] THEN
					WORDS_chart
				ELSE
					IF Input[1] IN ['b', 'B'] THEN
					BEGIN
						Method := 'insert';
						WORDS_update(Method);
					END
					ELSE
						IF Input[1] IN ['c', 'C'] THEN
						BEGIN
							Method := 'remove';
							WORDS_update(Method);
						END
						ELSE
							IF Input[1] IN ['x', 'X'] THEN
							BEGIN
								Mode := 'sort';
								FILES_io(Mode);
							END
							ELSE
							BEGIN
								Pcolor := 4;
								Ptype := 'ERROR';
								Ptext := 'UNKNOWN OPTION!';
								Pkey := TRUE;
								Prompt(Pcolor, Ptype, Ptext, Pkey);
							END;
			END;
		END;
	UNTIL Input[1] IN ['x', 'X'];
	WORDS_setting := TRUE;
END;

BEGIN
	IF Mode = 'switch' THEN
		WORDS_switch
	ELSE
		IF Mode = 'setting' THEN
			WORDS_setting;
	Mode := '';
END;

PROCEDURE Catalog;
VAR
	N : integer;
	Input : string;
BEGIN
	REPEAT
		USERS_locate(N);
		Pcolor := 7;
		Ptype := 'WELCOME';
		Ptext := 'MAIN MENU.';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		writeln;
		textbackground(1);
		writeln('>>> PLEASE SELECT <<<');
		textbackground(0);
		IF (USERS_array[N].Identity = 'guest') OR (USERS_array[N].Identity = 'teens') THEN
		BEGIN
			writeln('[a] PLAY NOW!');
			writeln('[b] PROFILE');
		END;
		writeln('[c] HIGH SCORE?');
		writeln('[d] USER SETTING');
		IF USERS_array[N].Identity = 'folks' THEN
			writeln('[e] DICT. SETTING');
		writeln('[x] QUIT');
		Pcolor := 7;
		Ptype := 'ASKING';
		Ptext := 'OPTION?';
		Pkey := FALSE;
		Prompt(Pcolor, Ptype, Ptext, Pkey);
		readln(Input);
		IF length(Input) = 0 THEN
		BEGIN
			Pcolor := 4;
			Ptype := 'ERROR';
			Ptext := 'EMPTY INPUT!';
			Pkey := TRUE;
			Prompt(Pcolor, Ptype, Ptext, Pkey);
		END
		ELSE
		BEGIN
			ISletter := TRUE;
			ISnumber := FALSE;
			ISroot := FALSE;
			Verify(Input[1], ISletter, ISnumber, ISroot);
			IF Valid THEN
				IF Input[1] IN ['a', 'A'] THEN
					IF (USERS_array[N].Identity = 'guest') OR (USERS_array[N].Identity = 'teens') THEN
					BEGIN
						clrscr;
						Mode := 'random';
						FILES_io(Mode);
						Mode := 'switch';
						WORDS_ui(Mode);
					END
					ELSE
					BEGIN
						Pcolor := 4;
						Ptype := 'ERROR';
						Ptext := 'UNKNOWN OPTION!';
						Pkey := TRUE;
						Prompt(Pcolor, Ptype, Ptext, Pkey);
					END
				ELSE
					IF Input[1] IN ['b', 'B'] THEN
						IF (USERS_array[N].Identity = 'guest') OR (USERS_array[N].Identity = 'teens') THEN
						BEGIN
							clrscr;
							Mode := 'profile';
							USERS_ui(Mode);
						END
						ELSE
						BEGIN
							Pcolor := 4;
							Ptype := 'ERROR';
							Ptext := 'UNKNOWN OPTION!';
							Pkey := TRUE;
							Prompt(Pcolor, Ptype, Ptext, Pkey);
						END
					ELSE
						IF Input[1] IN ['c', 'C'] THEN
						BEGIN
							clrscr;
							Mode := 'sort';
							FILES_io(Mode);
							Mode := 'chart';
							USERS_ui(Mode);
							Mode := 'random';
							FILES_io(Mode);
						END
						ELSE
							IF Input[1] IN ['d', 'D'] THEN
							BEGIN
								clrscr;
								Mode := 'setting';
								USERS_ui(Mode);
							END
							ELSE
								IF Input[1] IN ['e', 'E'] THEN
									IF USERS_array[N].Identity = 'folks' THEN
									BEGIN
										clrscr;
										Mode := 'setting';
										WORDS_ui(Mode);
									END
									ELSE
									BEGIN
										Pcolor := 4;
										Ptype := 'ERROR';
										Ptext := 'UNKNOWN OPTION!';
										Pkey := TRUE;
										Prompt(Pcolor, Ptype, Ptext, Pkey);
									END
								ELSE
									IF Input[1] IN ['x', 'X'] THEN
									BEGIN
										clrscr;
										Mode := 'signout';
										USERS_ui(Mode);
									END
									ELSE
									BEGIN
										Pcolor := 4;
										Ptype := 'ERROR';
										Ptext := 'UNKNOWN OPTION!';
										Pkey := TRUE;
										Prompt(Pcolor, Ptype, Ptext, Pkey);
									END;
		END;
	UNTIL Input[1] IN ['x', 'X'];
END;

BEGIN
	randomize;
	textbackground(0);
	clrscr;
	writeln;
	writeln('   ooooooooooo oooo   oooo  ooooooo8 ooooo       ooooo  oooooooo8 ooooo ooooo');
	writeln('    888    88   8888o  88 o888    88  888         888  888         888   888 ');
	writeln('    888ooo8     88 888o88 888    oooo 888         888   888oooooo  888ooo888 ');
	writeln('    888    oo   88   8888 888o    88  888      o  888          888 888   888 ');
	writeln('   o888ooo8888 o88o    88  888ooo888 o888ooooo88 o888o o88oooo888 o888o o888o');
	writeln('   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
	writeln;
	writeln('                                                 oooo       ');
	writeln('   oooo   oooo  ooooooo     ooooooo    ooooooo    888ooooo  ');
	writeln('    888   888 888     888 888     888  ooooo888   888    888');
	writeln('     888 888  888     888 888        888    888   888    888');
	writeln('       888      88ooo88     88ooo888  88ooo88 8o o888ooo88  ');
	writeln;
	writeln('                                      oooo        o88                        ');
	writeln('   oo ooo oooo    ooooooo    ooooooo   888ooooo   oooo  oo oooooo   oooooooo8');
	writeln('    888 888 888   ooooo888 888     888 888   888   888   888   888 888ooooo8 ');
	writeln('    888 888 888 888    888 888         888   888   888   888   888 888       ');
	writeln('   o888o888o888o 88ooo88 8o  88ooo888 o888o o888o o888o o888o o888o  88ooo888');
	writeln;
	write('   ');
	textbackground(7);
	textcolor(4);
	write(' [DEMO EDITION / v1.10 / COPYRIGHT 2011 CALVIN TAM / ALL RIGHTS RESERVED] ');
	textbackground(0);
	textcolor(7);
	delay(2000);
	Directory;
	Mode := 'inital';
	FILES_io(Mode);
	Mode := 'sort';
	FILES_io(Mode);
	REPEAT
		Mode := 'switch';
		USERS_ui(Mode);
		IF NOT (USERS_present = '') THEN
			Catalog;
	UNTIL USERS_present = '';
	Mode := 'sort';
	FILES_io(Mode);
	Mode := 'backup';
	FILES_io(Mode);
END.
